package pe.uni.alarreateguic.examen_final;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button botonVerdadero;
    private Button botonFalso;
    private Button botonSiguiente;
    private Button botonAnterior;
    private TextView preguntasTrivia;
    private Preguntas[] preguntaTrivia = new Preguntas[]{
            new Preguntas(R.string.Pregunta1),
            new Preguntas(R.string.Pregunta2),
            new Preguntas(R.string.Pregunta3),
            new Preguntas(R.string.Pregunta4),
            new Preguntas(R.string.Pregunta5),
            new Preguntas(R.string.Pregunta6),
};

private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonVerdadero = findViewById(R.id.buttonVerdadero);
        botonFalso = findViewById(R.id.buttonFalso);
        botonVerdadero.setText("Verdadero");
        botonFalso.setText("Falso");

        botonVerdadero = findViewById(R.id.buttonVerdadero);
        preguntasTrivia = findViewById(R.id.textViewPreguntas);
        botonVerdadero.setOnClickListener((view) -> {
            if (i == 0) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 1) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 2) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 3) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 4) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 5) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            }
        });
        botonFalso = findViewById(R.id.buttonFalso);
        botonFalso.setOnClickListener((view) -> {
            if (i == 0) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 1) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 2) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 3) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 4) {
                Toast toast = Toast.makeText(MainActivity.this,"Incorrecto. ¡Suerte a la próxima!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            } else if (i == 5) {
                Toast toast = Toast.makeText(MainActivity.this,"Correcto. ¡Felicidades!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,580);
                toast.show();
            }
        });

        botonSiguiente = findViewById(R.id.buttonSiguiente);
        botonSiguiente.setOnClickListener((view) -> {
            i = (i+1) % preguntaTrivia.length;
            updatePregunta();
            if (i == 0){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 1){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 2){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 3){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 4){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 5){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            }
        });

        updatePregunta();

        botonAnterior = findViewById(R.id.buttonAnterior);
        botonAnterior.setOnClickListener((view) -> {
            if (i > 0) {
                i = (i-1) % preguntaTrivia.length;
            } else {
                i = preguntaTrivia.length - 1;
            }
            updatePregunta();
            if (i == 0){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 1){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 2){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 3){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 4){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            } else if (i == 5){
                botonVerdadero.setText("Verdadero");
                botonFalso.setText("Falso");
            }
        });

        updatePregunta();
    }
    private void updatePregunta(){
        int pregunta = preguntaTrivia[i].getId_pregunta();
        preguntasTrivia.setText(pregunta);
    }
}